# jira-worklogger

The best way to time-track on jira


### Login page
<p align="center">
 <img src="https://github.com/emersonschulze/worktime/blob/master/examples/login.png"> </br>
</p>


### Time-track page
<p align="center">
 <img src="https://github.com/emersonschulze/worktime/blob/master/examples/example.png"> </br>
</p>


### Docker compose run  Configuração
<p align="center">
    - 'METEOR_SETTINGS={"jiraUrl":"http://yourcompanyjira.com","filter":"YourJiraFilter","email":"smtps://mail@gmail.com:password@smtp.gmail.com:465/","ldapUrl":"ldap://ldap.yourcompany.com","ldapPort":"389","dn":",ou=Users,dc=xxx,dc=xxx,dc=xxx"}'
</p>