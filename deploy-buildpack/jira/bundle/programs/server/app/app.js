var require = meteorInstall({"lib":{"collections":{"tabela.js":function module(require,exports,module){

//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
// lib/collections/tabela.js                                                            //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////
                                                                                        //
module.export({
  Tabela: () => Tabela
});
let Mongo;
module.link("meteor/mongo", {
  Mongo(v) {
    Mongo = v;
  }

}, 0);
const Tabela = new Mongo.Collection('tabela');

if (Meteor.isServer) {
  Meteor.publish('tabela', function tabelaPublication() {
    return Tabela.find();
  });
}
//////////////////////////////////////////////////////////////////////////////////////////

},"userBase.js":function module(require,exports,module){

//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
// lib/collections/userBase.js                                                          //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////
                                                                                        //
module.export({
  UserBase: () => UserBase
});
let Mongo;
module.link("meteor/mongo", {
  Mongo(v) {
    Mongo = v;
  }

}, 0);
const UserBase = new Mongo.Collection('userBase');

if (Meteor.isServer) {
  Meteor.publish('userBase', function userBasePublication() {
    return UserBase.find();
  });
}
//////////////////////////////////////////////////////////////////////////////////////////

},"works.js":function module(require,exports,module){

//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
// lib/collections/works.js                                                             //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////
                                                                                        //
module.export({
  Works: () => Works
});
let Mongo;
module.link("meteor/mongo", {
  Mongo(v) {
    Mongo = v;
  }

}, 0);
const Works = new Mongo.Collection('works');

if (Meteor.isServer) {
  Meteor.publish('works', function worksPublication() {
    return Works.find();
  });
}
//////////////////////////////////////////////////////////////////////////////////////////

}}},"server":{"main.js":function module(require,exports,module){

//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
// server/main.js                                                                       //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////
                                                                                        //
let Base64;
module.link("meteor/ostrio:base64", {
  Base64(v) {
    Base64 = v;
  }

}, 0);
let Meteor;
module.link("meteor/meteor", {
  Meteor(v) {
    Meteor = v;
  }

}, 1);
let HTTP;
module.link("meteor/http", {
  HTTP(v) {
    HTTP = v;
  }

}, 2);
let Mongo;
module.link("meteor/mongo", {
  Mongo(v) {
    Mongo = v;
  }

}, 3);
let userBase;
module.link("../lib/collections/userBase.js", {
  userBase(v) {
    userBase = v;
  }

}, 4);
let Tabela;
module.link("../lib/collections/tabela.js", {
  Tabela(v) {
    Tabela = v;
  }

}, 5);
let Works;
module.link("../lib/collections/works.js", {
  Works(v) {
    Works = v;
  }

}, 6);
let $;
module.link("meteor/jquery", {
  $(v) {
    $ = v;
  }

}, 7);
const base64 = new Base64();
Meteor.startup(() => {
  process.env.MAIL_URL = Meteor.settings.email;
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
  Accounts.emailTemplates.from = 'support_team@sancorseguros.com';
  Accounts.emailTemplates.siteName = 'JIRA WORKTIME';
  Accounts.emailTemplates.resetPassword = {
    subject(user) {
      return "Esqueci Senha";
    },

    text(user, url) {
      return "Ol\xE1! \n      Voc\xEA recebeu este email, porque solicitou a altera\xE7\xE3o de senha do Jira WorkTime.\n      Click no link abaixo para resetar a sua senha.\n\n      ".concat(url, "\n  \n      Caso voc\xEA n\xE3o solicitou, por favor ignore esta mensagem.\n  \n  Obrigado,\n  WorkTime Team.\n  ");
    },

    html(user, url) {}

  };
});
Meteor.methods({
  'logWork': function (userBase, issue, startedTime, tempoGasto, comentario) {
    const fetch = require('node-fetch');

    return fetch(Meteor.settings.jiraUrl + '/rest/api/2/issue/' + issue + '/worklog', {
      method: 'POST',
      headers: {
        'Authorization': 'Basic ' + userBase,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "timeSpentSeconds": tempoGasto,
        "started": startedTime,
        "comment": comentario
      })
    }).then(response => {
      if (response.status == 201) {
        return response.status;
      } else {
        return response.json();
      }
    }).catch(err => console.error(err));
  },
  'checkIfUserExists': function (username) {
    return Meteor.users.findOne({
      username: username
    }) ? true : false;
  },
  'getDn': function () {
    return Meteor.settings.dn;
  },
  'getUserIssues': function (userBase) {
    const fetch = require('node-fetch');

    return fetch(Meteor.settings.jiraUrl + '/rest/api/3/search?jql=assignee="' + userBase.user + '"' + Meteor.settings.filter, {
      method: 'GET',
      headers: {
        'Authorization': 'Basic ' + userBase.base,
        'Accept': 'application/json'
      }
    }).then(response => {
      return response.json();
    }).catch(err => console.error(err));
  },
  'removerIssues': function () {
    return Tabela.remove({});
  }
});
//////////////////////////////////////////////////////////////////////////////////////////

}}},{
  "extensions": [
    ".js",
    ".json",
    ".ts",
    ".mjs"
  ]
});

var exports = require("/server/main.js");
//# sourceURL=meteor://💻app/app/app.js
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1ldGVvcjovL/CfkrthcHAvbGliL2NvbGxlY3Rpb25zL3RhYmVsYS5qcyIsIm1ldGVvcjovL/CfkrthcHAvbGliL2NvbGxlY3Rpb25zL3VzZXJCYXNlLmpzIiwibWV0ZW9yOi8v8J+Su2FwcC9saWIvY29sbGVjdGlvbnMvd29ya3MuanMiLCJtZXRlb3I6Ly/wn5K7YXBwL3NlcnZlci9tYWluLmpzIl0sIm5hbWVzIjpbIm1vZHVsZSIsImV4cG9ydCIsIlRhYmVsYSIsIk1vbmdvIiwibGluayIsInYiLCJDb2xsZWN0aW9uIiwiTWV0ZW9yIiwiaXNTZXJ2ZXIiLCJwdWJsaXNoIiwidGFiZWxhUHVibGljYXRpb24iLCJmaW5kIiwiVXNlckJhc2UiLCJ1c2VyQmFzZVB1YmxpY2F0aW9uIiwiV29ya3MiLCJ3b3Jrc1B1YmxpY2F0aW9uIiwiQmFzZTY0IiwiSFRUUCIsInVzZXJCYXNlIiwiJCIsImJhc2U2NCIsInN0YXJ0dXAiLCJwcm9jZXNzIiwiZW52IiwiTUFJTF9VUkwiLCJzZXR0aW5ncyIsImVtYWlsIiwiTk9ERV9UTFNfUkVKRUNUX1VOQVVUSE9SSVpFRCIsIkFjY291bnRzIiwiZW1haWxUZW1wbGF0ZXMiLCJmcm9tIiwic2l0ZU5hbWUiLCJyZXNldFBhc3N3b3JkIiwic3ViamVjdCIsInVzZXIiLCJ0ZXh0IiwidXJsIiwiaHRtbCIsIm1ldGhvZHMiLCJpc3N1ZSIsInN0YXJ0ZWRUaW1lIiwidGVtcG9HYXN0byIsImNvbWVudGFyaW8iLCJmZXRjaCIsInJlcXVpcmUiLCJqaXJhVXJsIiwibWV0aG9kIiwiaGVhZGVycyIsImJvZHkiLCJKU09OIiwic3RyaW5naWZ5IiwidGhlbiIsInJlc3BvbnNlIiwic3RhdHVzIiwianNvbiIsImNhdGNoIiwiZXJyIiwiY29uc29sZSIsImVycm9yIiwidXNlcm5hbWUiLCJ1c2VycyIsImZpbmRPbmUiLCJkbiIsImZpbHRlciIsImJhc2UiLCJyZW1vdmUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUFBLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjO0FBQUNDLFFBQU0sRUFBQyxNQUFJQTtBQUFaLENBQWQ7QUFBbUMsSUFBSUMsS0FBSjtBQUFVSCxNQUFNLENBQUNJLElBQVAsQ0FBWSxjQUFaLEVBQTJCO0FBQUNELE9BQUssQ0FBQ0UsQ0FBRCxFQUFHO0FBQUNGLFNBQUssR0FBQ0UsQ0FBTjtBQUFROztBQUFsQixDQUEzQixFQUErQyxDQUEvQztBQUV0QyxNQUFNSCxNQUFNLEdBQUcsSUFBSUMsS0FBSyxDQUFDRyxVQUFWLENBQXFCLFFBQXJCLENBQWY7O0FBRVAsSUFBSUMsTUFBTSxDQUFDQyxRQUFYLEVBQXFCO0FBQ2pCRCxRQUFNLENBQUNFLE9BQVAsQ0FBZSxRQUFmLEVBQXlCLFNBQVNDLGlCQUFULEdBQTZCO0FBQ3BELFdBQU9SLE1BQU0sQ0FBQ1MsSUFBUCxFQUFQO0FBQ0QsR0FGRDtBQUdILEM7Ozs7Ozs7Ozs7O0FDUkRYLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjO0FBQUNXLFVBQVEsRUFBQyxNQUFJQTtBQUFkLENBQWQ7QUFBdUMsSUFBSVQsS0FBSjtBQUFVSCxNQUFNLENBQUNJLElBQVAsQ0FBWSxjQUFaLEVBQTJCO0FBQUNELE9BQUssQ0FBQ0UsQ0FBRCxFQUFHO0FBQUNGLFNBQUssR0FBQ0UsQ0FBTjtBQUFROztBQUFsQixDQUEzQixFQUErQyxDQUEvQztBQUUxQyxNQUFNTyxRQUFRLEdBQUcsSUFBSVQsS0FBSyxDQUFDRyxVQUFWLENBQXFCLFVBQXJCLENBQWpCOztBQUVQLElBQUlDLE1BQU0sQ0FBQ0MsUUFBWCxFQUFxQjtBQUNqQkQsUUFBTSxDQUFDRSxPQUFQLENBQWUsVUFBZixFQUEyQixTQUFTSSxtQkFBVCxHQUErQjtBQUN4RCxXQUFPRCxRQUFRLENBQUNELElBQVQsRUFBUDtBQUNELEdBRkQ7QUFHSCxDOzs7Ozs7Ozs7OztBQ1JEWCxNQUFNLENBQUNDLE1BQVAsQ0FBYztBQUFDYSxPQUFLLEVBQUMsTUFBSUE7QUFBWCxDQUFkO0FBQWlDLElBQUlYLEtBQUo7QUFBVUgsTUFBTSxDQUFDSSxJQUFQLENBQVksY0FBWixFQUEyQjtBQUFDRCxPQUFLLENBQUNFLENBQUQsRUFBRztBQUFDRixTQUFLLEdBQUNFLENBQU47QUFBUTs7QUFBbEIsQ0FBM0IsRUFBK0MsQ0FBL0M7QUFFcEMsTUFBTVMsS0FBSyxHQUFHLElBQUlYLEtBQUssQ0FBQ0csVUFBVixDQUFxQixPQUFyQixDQUFkOztBQUVQLElBQUlDLE1BQU0sQ0FBQ0MsUUFBWCxFQUFxQjtBQUNqQkQsUUFBTSxDQUFDRSxPQUFQLENBQWUsT0FBZixFQUF3QixTQUFTTSxnQkFBVCxHQUE0QjtBQUNsRCxXQUFPRCxLQUFLLENBQUNILElBQU4sRUFBUDtBQUNELEdBRkQ7QUFHSCxDOzs7Ozs7Ozs7OztBQ1JELElBQUlLLE1BQUo7QUFBV2hCLE1BQU0sQ0FBQ0ksSUFBUCxDQUFZLHNCQUFaLEVBQW1DO0FBQUNZLFFBQU0sQ0FBQ1gsQ0FBRCxFQUFHO0FBQUNXLFVBQU0sR0FBQ1gsQ0FBUDtBQUFTOztBQUFwQixDQUFuQyxFQUF5RCxDQUF6RDtBQUE0RCxJQUFJRSxNQUFKO0FBQVdQLE1BQU0sQ0FBQ0ksSUFBUCxDQUFZLGVBQVosRUFBNEI7QUFBQ0csUUFBTSxDQUFDRixDQUFELEVBQUc7QUFBQ0UsVUFBTSxHQUFDRixDQUFQO0FBQVM7O0FBQXBCLENBQTVCLEVBQWtELENBQWxEO0FBQXFELElBQUlZLElBQUo7QUFBU2pCLE1BQU0sQ0FBQ0ksSUFBUCxDQUFZLGFBQVosRUFBMEI7QUFBQ2EsTUFBSSxDQUFDWixDQUFELEVBQUc7QUFBQ1ksUUFBSSxHQUFDWixDQUFMO0FBQU87O0FBQWhCLENBQTFCLEVBQTRDLENBQTVDO0FBQStDLElBQUlGLEtBQUo7QUFBVUgsTUFBTSxDQUFDSSxJQUFQLENBQVksY0FBWixFQUEyQjtBQUFDRCxPQUFLLENBQUNFLENBQUQsRUFBRztBQUFDRixTQUFLLEdBQUNFLENBQU47QUFBUTs7QUFBbEIsQ0FBM0IsRUFBK0MsQ0FBL0M7QUFBa0QsSUFBSWEsUUFBSjtBQUFhbEIsTUFBTSxDQUFDSSxJQUFQLENBQVksZ0NBQVosRUFBNkM7QUFBQ2MsVUFBUSxDQUFDYixDQUFELEVBQUc7QUFBQ2EsWUFBUSxHQUFDYixDQUFUO0FBQVc7O0FBQXhCLENBQTdDLEVBQXVFLENBQXZFO0FBQTBFLElBQUlILE1BQUo7QUFBV0YsTUFBTSxDQUFDSSxJQUFQLENBQVksOEJBQVosRUFBMkM7QUFBQ0YsUUFBTSxDQUFDRyxDQUFELEVBQUc7QUFBQ0gsVUFBTSxHQUFDRyxDQUFQO0FBQVM7O0FBQXBCLENBQTNDLEVBQWlFLENBQWpFO0FBQW9FLElBQUlTLEtBQUo7QUFBVWQsTUFBTSxDQUFDSSxJQUFQLENBQVksNkJBQVosRUFBMEM7QUFBQ1UsT0FBSyxDQUFDVCxDQUFELEVBQUc7QUFBQ1MsU0FBSyxHQUFDVCxDQUFOO0FBQVE7O0FBQWxCLENBQTFDLEVBQThELENBQTlEO0FBQWlFLElBQUljLENBQUo7QUFBTW5CLE1BQU0sQ0FBQ0ksSUFBUCxDQUFZLGVBQVosRUFBNEI7QUFBQ2UsR0FBQyxDQUFDZCxDQUFELEVBQUc7QUFBQ2MsS0FBQyxHQUFDZCxDQUFGO0FBQUk7O0FBQVYsQ0FBNUIsRUFBd0MsQ0FBeEM7QUFRbGYsTUFBTWUsTUFBTSxHQUFHLElBQUlKLE1BQUosRUFBZjtBQUVBVCxNQUFNLENBQUNjLE9BQVAsQ0FBZSxNQUFNO0FBQ25CQyxTQUFPLENBQUNDLEdBQVIsQ0FBWUMsUUFBWixHQUF1QmpCLE1BQU0sQ0FBQ2tCLFFBQVAsQ0FBZ0JDLEtBQXZDO0FBQ0FKLFNBQU8sQ0FBQ0MsR0FBUixDQUFZSSw0QkFBWixHQUEyQyxDQUEzQztBQUVBQyxVQUFRLENBQUNDLGNBQVQsQ0FBd0JDLElBQXhCLEdBQStCLGdDQUEvQjtBQUNBRixVQUFRLENBQUNDLGNBQVQsQ0FBd0JFLFFBQXhCLEdBQW1DLGVBQW5DO0FBRUFILFVBQVEsQ0FBQ0MsY0FBVCxDQUF3QkcsYUFBeEIsR0FBd0M7QUFDdENDLFdBQU8sQ0FBQ0MsSUFBRCxFQUFPO0FBQ1osYUFBTyxlQUFQO0FBQ0QsS0FIcUM7O0FBSXRDQyxRQUFJLENBQUNELElBQUQsRUFBT0UsR0FBUCxFQUFZO0FBQ2QsZ01BSUVBLEdBSkY7QUFXRCxLQWhCcUM7O0FBaUJ0Q0MsUUFBSSxDQUFDSCxJQUFELEVBQU9FLEdBQVAsRUFBWSxDQUNmOztBQWxCcUMsR0FBeEM7QUFxQkQsQ0E1QkQ7QUE4QkE3QixNQUFNLENBQUMrQixPQUFQLENBQWU7QUFDYixhQUFXLFVBQVVwQixRQUFWLEVBQW9CcUIsS0FBcEIsRUFBMkJDLFdBQTNCLEVBQXdDQyxVQUF4QyxFQUFvREMsVUFBcEQsRUFBZ0U7QUFDekUsVUFBTUMsS0FBSyxHQUFHQyxPQUFPLENBQUMsWUFBRCxDQUFyQjs7QUFDQSxXQUFPRCxLQUFLLENBQUNwQyxNQUFNLENBQUNrQixRQUFQLENBQWdCb0IsT0FBaEIsR0FBMEIsb0JBQTFCLEdBQWlETixLQUFqRCxHQUF5RCxVQUExRCxFQUFzRTtBQUNoRk8sWUFBTSxFQUFFLE1BRHdFO0FBRWhGQyxhQUFPLEVBQUU7QUFDUCx5QkFBaUIsV0FBVzdCLFFBRHJCO0FBRVAsd0JBQWdCO0FBRlQsT0FGdUU7QUFNaEY4QixVQUFJLEVBQUVDLElBQUksQ0FBQ0MsU0FBTCxDQUFlO0FBQUUsNEJBQW9CVCxVQUF0QjtBQUFrQyxtQkFBV0QsV0FBN0M7QUFBMEQsbUJBQVdFO0FBQXJFLE9BQWY7QUFOMEUsS0FBdEUsQ0FBTCxDQVFKUyxJQVJJLENBUUNDLFFBQVEsSUFBSTtBQUNoQixVQUFJQSxRQUFRLENBQUNDLE1BQVQsSUFBbUIsR0FBdkIsRUFBNEI7QUFDMUIsZUFBT0QsUUFBUSxDQUFDQyxNQUFoQjtBQUNELE9BRkQsTUFFTztBQUNMLGVBQU9ELFFBQVEsQ0FBQ0UsSUFBVCxFQUFQO0FBQ0Q7QUFFRixLQWZJLEVBZ0JKQyxLQWhCSSxDQWdCRUMsR0FBRyxJQUFJQyxPQUFPLENBQUNDLEtBQVIsQ0FBY0YsR0FBZCxDQWhCVCxDQUFQO0FBaUJELEdBcEJZO0FBc0JiLHVCQUFxQixVQUFVRyxRQUFWLEVBQW9CO0FBQ3ZDLFdBQVFwRCxNQUFNLENBQUNxRCxLQUFQLENBQWFDLE9BQWIsQ0FBcUI7QUFBRUYsY0FBUSxFQUFFQTtBQUFaLEtBQXJCLENBQUQsR0FBaUQsSUFBakQsR0FBd0QsS0FBL0Q7QUFDRCxHQXhCWTtBQTBCYixXQUFTLFlBQVk7QUFDbkIsV0FBT3BELE1BQU0sQ0FBQ2tCLFFBQVAsQ0FBZ0JxQyxFQUF2QjtBQUNELEdBNUJZO0FBOEJiLG1CQUFpQixVQUFVNUMsUUFBVixFQUFvQjtBQUVuQyxVQUFNeUIsS0FBSyxHQUFHQyxPQUFPLENBQUMsWUFBRCxDQUFyQjs7QUFDQSxXQUFPRCxLQUFLLENBQUNwQyxNQUFNLENBQUNrQixRQUFQLENBQWdCb0IsT0FBaEIsR0FBMEIsbUNBQTFCLEdBQWdFM0IsUUFBUSxDQUFDZ0IsSUFBekUsR0FBZ0YsR0FBaEYsR0FBc0YzQixNQUFNLENBQUNrQixRQUFQLENBQWdCc0MsTUFBdkcsRUFBK0c7QUFDekhqQixZQUFNLEVBQUUsS0FEaUg7QUFFekhDLGFBQU8sRUFBRTtBQUNQLHlCQUFpQixXQUFXN0IsUUFBUSxDQUFDOEMsSUFEOUI7QUFFUCxrQkFBVTtBQUZIO0FBRmdILEtBQS9HLENBQUwsQ0FPSmIsSUFQSSxDQU9DQyxRQUFRLElBQUk7QUFFaEIsYUFBT0EsUUFBUSxDQUFDRSxJQUFULEVBQVA7QUFDRCxLQVZJLEVBV0pDLEtBWEksQ0FXRUMsR0FBRyxJQUFJQyxPQUFPLENBQUNDLEtBQVIsQ0FBY0YsR0FBZCxDQVhULENBQVA7QUFZRCxHQTdDWTtBQStDYixtQkFBaUIsWUFBWTtBQUMzQixXQUFPdEQsTUFBTSxDQUFDK0QsTUFBUCxDQUFjLEVBQWQsQ0FBUDtBQUNEO0FBakRZLENBQWYsRSIsImZpbGUiOiIvYXBwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTW9uZ28gfSBmcm9tICdtZXRlb3IvbW9uZ28nO1xyXG5cclxuZXhwb3J0IGNvbnN0IFRhYmVsYSA9IG5ldyBNb25nby5Db2xsZWN0aW9uKCd0YWJlbGEnKTtcclxuXHJcbmlmIChNZXRlb3IuaXNTZXJ2ZXIpIHtcclxuICAgIE1ldGVvci5wdWJsaXNoKCd0YWJlbGEnLCBmdW5jdGlvbiB0YWJlbGFQdWJsaWNhdGlvbigpIHtcclxuICAgICAgcmV0dXJuIFRhYmVsYS5maW5kKCk7XHJcbiAgICB9KTtcclxufVxyXG4iLCJpbXBvcnQgeyBNb25nbyB9IGZyb20gJ21ldGVvci9tb25nbyc7XHJcblxyXG5leHBvcnQgY29uc3QgVXNlckJhc2UgPSBuZXcgTW9uZ28uQ29sbGVjdGlvbigndXNlckJhc2UnKTtcclxuXHJcbmlmIChNZXRlb3IuaXNTZXJ2ZXIpIHtcclxuICAgIE1ldGVvci5wdWJsaXNoKCd1c2VyQmFzZScsIGZ1bmN0aW9uIHVzZXJCYXNlUHVibGljYXRpb24oKSB7XHJcbiAgICAgIHJldHVybiBVc2VyQmFzZS5maW5kKCk7XHJcbiAgICB9KTtcclxufVxyXG4iLCJpbXBvcnQgeyBNb25nbyB9IGZyb20gJ21ldGVvci9tb25nbyc7XHJcblxyXG5leHBvcnQgY29uc3QgV29ya3MgPSBuZXcgTW9uZ28uQ29sbGVjdGlvbignd29ya3MnKTtcclxuXHJcbmlmIChNZXRlb3IuaXNTZXJ2ZXIpIHtcclxuICAgIE1ldGVvci5wdWJsaXNoKCd3b3JrcycsIGZ1bmN0aW9uIHdvcmtzUHVibGljYXRpb24oKSB7XHJcbiAgICAgIHJldHVybiBXb3Jrcy5maW5kKCk7XHJcbiAgICB9KTtcclxufVxyXG4iLCJpbXBvcnQgeyBCYXNlNjQgfSBmcm9tICdtZXRlb3Ivb3N0cmlvOmJhc2U2NCc7XHJcbmltcG9ydCB7IE1ldGVvciB9IGZyb20gJ21ldGVvci9tZXRlb3InO1xyXG5pbXBvcnQgeyBIVFRQIH0gZnJvbSAnbWV0ZW9yL2h0dHAnO1xyXG5pbXBvcnQgeyBNb25nbyB9IGZyb20gJ21ldGVvci9tb25nbyc7XHJcbmltcG9ydCB7IHVzZXJCYXNlIH0gZnJvbSAnLi4vbGliL2NvbGxlY3Rpb25zL3VzZXJCYXNlLmpzJztcclxuaW1wb3J0IHsgVGFiZWxhIH0gZnJvbSAnLi4vbGliL2NvbGxlY3Rpb25zL3RhYmVsYS5qcyc7XHJcbmltcG9ydCB7IFdvcmtzIH0gZnJvbSAnLi4vbGliL2NvbGxlY3Rpb25zL3dvcmtzLmpzJztcclxuaW1wb3J0IHsgJCB9IGZyb20gJ21ldGVvci9qcXVlcnknXHJcbmNvbnN0IGJhc2U2NCA9IG5ldyBCYXNlNjQoKTtcclxuXHJcbk1ldGVvci5zdGFydHVwKCgpID0+IHtcclxuICBwcm9jZXNzLmVudi5NQUlMX1VSTCA9IE1ldGVvci5zZXR0aW5ncy5lbWFpbDtcclxuICBwcm9jZXNzLmVudi5OT0RFX1RMU19SRUpFQ1RfVU5BVVRIT1JJWkVEID0gMDtcclxuXHJcbiAgQWNjb3VudHMuZW1haWxUZW1wbGF0ZXMuZnJvbSA9ICdzdXBwb3J0X3RlYW1Ac2FuY29yc2VndXJvcy5jb20nO1xyXG4gIEFjY291bnRzLmVtYWlsVGVtcGxhdGVzLnNpdGVOYW1lID0gJ0pJUkEgV09SS1RJTUUnO1xyXG5cclxuICBBY2NvdW50cy5lbWFpbFRlbXBsYXRlcy5yZXNldFBhc3N3b3JkID0ge1xyXG4gICAgc3ViamVjdCh1c2VyKSB7XHJcbiAgICAgIHJldHVybiBcIkVzcXVlY2kgU2VuaGFcIjtcclxuICAgIH0sXHJcbiAgICB0ZXh0KHVzZXIsIHVybCkge1xyXG4gICAgICByZXR1cm4gYE9sw6EhIFxyXG4gICAgICBWb2PDqiByZWNlYmV1IGVzdGUgZW1haWwsIHBvcnF1ZSBzb2xpY2l0b3UgYSBhbHRlcmHDp8OjbyBkZSBzZW5oYSBkbyBKaXJhIFdvcmtUaW1lLlxyXG4gICAgICBDbGljayBubyBsaW5rIGFiYWl4byBwYXJhIHJlc2V0YXIgYSBzdWEgc2VuaGEuXHJcblxyXG4gICAgICAke3VybH1cclxuICBcclxuICAgICAgQ2FzbyB2b2PDqiBuw6NvIHNvbGljaXRvdSwgcG9yIGZhdm9yIGlnbm9yZSBlc3RhIG1lbnNhZ2VtLlxyXG4gIFxyXG4gIE9icmlnYWRvLFxyXG4gIFdvcmtUaW1lIFRlYW0uXHJcbiAgYFxyXG4gICAgfSxcclxuICAgIGh0bWwodXNlciwgdXJsKSB7XHJcbiAgICB9XHJcbiAgfTtcclxuXHJcbn0pO1xyXG5cclxuTWV0ZW9yLm1ldGhvZHMoe1xyXG4gICdsb2dXb3JrJzogZnVuY3Rpb24gKHVzZXJCYXNlLCBpc3N1ZSwgc3RhcnRlZFRpbWUsIHRlbXBvR2FzdG8sIGNvbWVudGFyaW8pIHtcclxuICAgIGNvbnN0IGZldGNoID0gcmVxdWlyZSgnbm9kZS1mZXRjaCcpO1xyXG4gICAgcmV0dXJuIGZldGNoKE1ldGVvci5zZXR0aW5ncy5qaXJhVXJsICsgJy9yZXN0L2FwaS8yL2lzc3VlLycgKyBpc3N1ZSArICcvd29ya2xvZycsIHtcclxuICAgICAgbWV0aG9kOiAnUE9TVCcsXHJcbiAgICAgIGhlYWRlcnM6IHtcclxuICAgICAgICAnQXV0aG9yaXphdGlvbic6ICdCYXNpYyAnICsgdXNlckJhc2UsXHJcbiAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyxcclxuICAgICAgfSxcclxuICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoeyBcInRpbWVTcGVudFNlY29uZHNcIjogdGVtcG9HYXN0bywgXCJzdGFydGVkXCI6IHN0YXJ0ZWRUaW1lLCBcImNvbW1lbnRcIjogY29tZW50YXJpbyB9KVxyXG4gICAgfSlcclxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgIGlmIChyZXNwb25zZS5zdGF0dXMgPT0gMjAxKSB7XHJcbiAgICAgICAgICByZXR1cm4gcmVzcG9uc2Uuc3RhdHVzO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICByZXR1cm4gcmVzcG9uc2UuanNvbigpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgIH0pXHJcbiAgICAgIC5jYXRjaChlcnIgPT4gY29uc29sZS5lcnJvcihlcnIpKTtcclxuICB9LFxyXG5cclxuICAnY2hlY2tJZlVzZXJFeGlzdHMnOiBmdW5jdGlvbiAodXNlcm5hbWUpIHtcclxuICAgIHJldHVybiAoTWV0ZW9yLnVzZXJzLmZpbmRPbmUoeyB1c2VybmFtZTogdXNlcm5hbWUgfSkpID8gdHJ1ZSA6IGZhbHNlO1xyXG4gIH0sXHJcblxyXG4gICdnZXREbic6IGZ1bmN0aW9uICgpIHtcclxuICAgIHJldHVybiBNZXRlb3Iuc2V0dGluZ3MuZG47XHJcbiAgfSxcclxuXHJcbiAgJ2dldFVzZXJJc3N1ZXMnOiBmdW5jdGlvbiAodXNlckJhc2UpIHtcclxuXHJcbiAgICBjb25zdCBmZXRjaCA9IHJlcXVpcmUoJ25vZGUtZmV0Y2gnKTtcclxuICAgIHJldHVybiBmZXRjaChNZXRlb3Iuc2V0dGluZ3MuamlyYVVybCArICcvcmVzdC9hcGkvMy9zZWFyY2g/anFsPWFzc2lnbmVlPVwiJyArIHVzZXJCYXNlLnVzZXIgKyAnXCInICsgTWV0ZW9yLnNldHRpbmdzLmZpbHRlciwge1xyXG4gICAgICBtZXRob2Q6ICdHRVQnLFxyXG4gICAgICBoZWFkZXJzOiB7XHJcbiAgICAgICAgJ0F1dGhvcml6YXRpb24nOiAnQmFzaWMgJyArIHVzZXJCYXNlLmJhc2UsXHJcbiAgICAgICAgJ0FjY2VwdCc6ICdhcHBsaWNhdGlvbi9qc29uJ1xyXG4gICAgICB9XHJcbiAgICB9KVxyXG4gICAgICAudGhlbihyZXNwb25zZSA9PiB7XHJcblxyXG4gICAgICAgIHJldHVybiByZXNwb25zZS5qc29uKCk7XHJcbiAgICAgIH0pXHJcbiAgICAgIC5jYXRjaChlcnIgPT4gY29uc29sZS5lcnJvcihlcnIpKTtcclxuICB9LFxyXG5cclxuICAncmVtb3Zlcklzc3Vlcyc6IGZ1bmN0aW9uICgpIHtcclxuICAgIHJldHVybiBUYWJlbGEucmVtb3ZlKHt9KTtcclxuICB9LFxyXG5cclxufSk7XHJcbiJdfQ==
