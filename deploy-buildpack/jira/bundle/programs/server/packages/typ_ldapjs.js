(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;

/* Package-scope variables */
var MeteorWrapperLdapjs;

(function(){

///////////////////////////////////////////////////////////////////////
//                                                                   //
// packages/typ_ldapjs/lib/ldapjs.js                                 //
//                                                                   //
///////////////////////////////////////////////////////////////////////
                                                                     //
MeteorWrapperLdapjs = Npm.require('ldapjs');
///////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
Package._define("typ:ldapjs", {
  MeteorWrapperLdapjs: MeteorWrapperLdapjs
});

})();
