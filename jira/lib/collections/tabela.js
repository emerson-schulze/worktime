import { Mongo } from 'meteor/mongo';

export const Tabela = new Mongo.Collection('tabela');

if (Meteor.isServer) {
    Meteor.publish('tabela', function tabelaPublication() {
      return Tabela.find();
    });
}
