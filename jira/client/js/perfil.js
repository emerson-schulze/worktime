import { UserBase } from '../../lib/collections/userBase.js';
import { Tabela } from '../../lib/collections/tabela.js';
import { Base64 } from 'meteor/ostrio:base64';
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { HTTP } from 'meteor/http';
import { Accounts } from 'meteor/accounts-base'

const base64 = new Base64();
Template.perfil.events({
  'submit form': function (event) {
    event.preventDefault();
    var newTokenVar = event.target.newtoken.value;
    var usuario = UserBase.findOne({ user: Meteor.user().username });
    UserBase.update({ _id: usuario._id }, { $set: { base: base64.encode(usuario.email + ':' + newTokenVar) } })
    Materialize.toast('Token Alterado com sucesso', 4000, 'rounded');
    Router.go("/");
  },
});

Template.perfil.onRendered(function bodyOnCreated() {
  $('ul.tabs').tabs();
});

Template.demanda.rendered = function () {
}

Template.demanda.helpers({
  'record': function () {
    return Tabela.find({}, { sort: { prioridade: 1 } }).fetch();
  },
});
