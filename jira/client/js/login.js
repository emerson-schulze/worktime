import { UserBase } from '../../lib/collections/userBase.js';
import { Tabela } from '../../lib/collections/tabela.js';
import { Base64 } from 'meteor/ostrio:base64';
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { HTTP } from 'meteor/http';
import { Accounts } from 'meteor/accounts-base'


UserIssues = [];
const base64 = new Base64();

loadUserIssues = function () {
  setTimeout(function () {

    if (Meteor.user()) {
      Meteor.call('getUserIssues', UserBase.find({ user: Meteor.user().username }).fetch()[0], (error, response) => {
        if (error) {
          Materialize.toast("Erro ao buscas issues:" + error, 4000, 'rounded');
        } else {
          if (response === undefined) {
            Materialize.toast("Erro ao buscas issues, atualizar o Jira Token", 4000, 'rounded');
          }

          if (response.total === 0) {
            Materialize.toast("Não foram encontradas issues em desenvolvimento, associadas ao usuário: " + UserBase.findOne({ user: Meteor.user().username }), 4000, 'rounded');
          }

          response['issues'].forEach(e => {
            UserIssues.push(e['key'] + " - " + e['fields']['summary']);
          });

          var temDados = Tabela.find().fetch();
          if (temDados.length !== 0)
            Meteor.call('removerIssues')

          response['issues'].forEach(data => {
            var tempoS = data['fields']['timespent'];
            tempoS = Number(tempoS);
            var h = Math.floor(tempoS / 3600);
            var m = Math.floor(tempoS % 3600 / 60);
            gasto = ('0' + h).slice(-2) + ":" + ('0' + m).slice(-2);

            Tabela.insert({
              issue: data['key'],
              descricao: data['fields']['summary'],
              tempo: gasto,
              status: data['fields']['status']['name'],
              tipo: data['fields']['issuetype']['name'],
              prioridade: data['fields']['priority']['name']
            });
          });

          $('input.autocomplete').autocomplete({
            source: UserIssues,
            limit: 10,
            minLength: 0
          });
        }
      });
    }
  }, 1000);
}

Template.loginAccount.events({
  'submit form'(event) {
    event.preventDefault();
    var loginVar = event.target.login.value;
    var passwordVar = event.target.password.value;

    Meteor.loginWithPassword(loginVar, passwordVar, function (error) {
      if (error) {
        Materialize.toast(error, 4000, 'rounded');
      } else {
        loadUserIssues();
        Router.go("worklogger");
      }
    });
  },

  'click #register': function (event) {
    event.preventDefault();
    Router.go("register");
  },

  'click #forgotPass': function (event) {
    event.preventDefault();
    Router.go("resetpass");
  }
});

Template.login.onRendered(function bodyOnCreated() {
  $('ul.tabs').tabs();
});

Template.register.events({
  'click #btn-div': function () {
    if ($('#video')[0].style.display === 'block') {
      $('#video')[0].style.display = 'none';
    } else {
      $('#video')[0].style.display = 'block';
    }
  },

  'submit form': function (event) {
    event.preventDefault();
    var loginVar = event.target.email.value;
    var passwordVar = event.target.password.value;
    var emailVar = event.target.email.value;
    var jiraVar = event.target.jiratoken.value

    Meteor.call('checkIfUserExists', loginVar, function (err, result) {
      if (err) {
        Materialize.toast('Erro ao verificar se existe o username', 4000, 'rounded');
      } else {
        if (result === false) {
          Accounts.createUser({
            email: emailVar,
            username: loginVar,
            password: passwordVar,
            jiratoken: jiraVar
          });
          UserBase.insert({
            base: base64.encode(emailVar + ':' + jiraVar),
            user: loginVar,
            email: emailVar
          });
          Materialize.toast('Usuário criado com sucesso', 4000, 'rounded');
        } else {
          Materialize.toast('Username já existe na base!', 4000, 'rounded');
        }
      }
    });
    Router.go("/");
  },
  'click #forgotPass': function (event) {
    var email = $('#email')[0].value

    if (email === "") {
      Materialize.toast('Campo e-mail vazio!', 4000);
      return;
    }

    var options = {};
    options.email = email;
    Accounts.forgotPassword(options, function (error) {
      if (error) {
        Materialize.toast('Erro ao resetar email, erro:' + error, 4000);
      } else {
        Materialize.toast('Verificar seu email!', 4000);
      }
    });
  },

});

Template.resetpass.events({
  'click #forgot': function (event) {
    var email = $('#email')[0].value

    if (email === "") {
      Materialize.toast('Campo e-mail vazio!', 4000);
      return;
    }

    Accounts.forgotPassword({ email: email }, function (error, r) {
      if (error) {
        if (error.message === 'User not found [403]') {
          Materialize.toast('Usuário não encontrado!', 4000);
        } else {
          Materialize.toast('Houve um problema, por favor tente mais tarde', 4000);
        }
      } else {
        Materialize.toast('Email enviado, verificar seu email!', 4000);
        Router.go("/");

        $('.modal').modal({
          opacity: .5,
          inDuration: 300,
          complete: function () {
            Router.go("/");
          }
        }
        );
        $('#modal1').modal('open');
      }
    });
  },
});
