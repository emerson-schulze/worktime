import { Works } from '../../lib/collections/works.js';
import { UserBase } from '../../lib/collections/userBase.js';
import { Mongo } from 'meteor/mongo';

Template.worklogger.helpers({
  works() {
    var user = Meteor.user();
    if (user) {
      return Works.find({ user: Meteor.user().username }, { sort: { startDate: 1 } });
    }
  },
  dateWithLog() {
    var user = Meteor.user();
    if (user) {
      var groups = _.groupBy(Works.find({ user: Meteor.user().username }, { sort: { startDate: 1 } }).fetch(), function (item) {
        return item.startDate.withoutTime();
      });

      return _.toArray(groups);
    }
  }
});

Template.worklogger.onCreated(function () {
  if (UserIssues && UserIssues !== 'null' && UserIssues !== 'undefined') {
    loadUserIssues();
  }
});

Template.worklogger.events({
  'click #enviar'(event) {
    var userLogin = Meteor.user().username;
    var userBase = UserBase.find({ user: userLogin }).fetch()[0].base;
    Works.find({ user: userLogin }).forEach(work => {
      var diff = Math.abs(work.startDate - work.endDate);
      var seconds = Math.floor((diff / 1000));

      if (seconds > 0 && work.startDate < work.endDate && work.issue) {
        var start = String(moment(work.startDate).utc().format("YYYY-MM-DDTHH:mm:ss.SSSZZ"));

        Meteor.call('logWork', userBase, work.issue, start, seconds, work.comment, (error, result) => {
          if (error) {
            Materialize.toast('Falha ao enviar a issue: ' + work.issue, 4000, 'rounded');
          }
          if (result == 201) {
            Works.remove(work._id);
            Materialize.toast('Apontamento de horas efetuado com sucesso na issues: ' + work.issue, 4000, 'rounded');
          } else {
            Materialize.toast('Erro ao enviar issues: ' + work.issue + ', Erro: ' + result.errorMessages, 4000, 'rounded');
          }
        });
      } else {
        if (work.issue.trim() == '') {
          Materialize.toast('Selecionar uma issue valida!', 4000, 'rounded');
        } else {
          Materialize.toast('Ajustar tempo inicio e fim da issue: ' + work.issue, 4000, 'rounded');
        }

      }
    });
  },

  'click #new_work'(event) {
    event.preventDefault();
    Works.insert({
      issue: '',
      startDate: new Date(),
      endDate: new Date(),
      user: Meteor.user().username,
      comment: '',
    });
  }
});
