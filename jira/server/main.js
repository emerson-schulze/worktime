import { Base64 } from 'meteor/ostrio:base64';
import { Meteor } from 'meteor/meteor';
import { HTTP } from 'meteor/http';
import { Mongo } from 'meteor/mongo';
import { userBase } from '../lib/collections/userBase.js';
import { Tabela } from '../lib/collections/tabela.js';
import { Works } from '../lib/collections/works.js';
import { $ } from 'meteor/jquery'
const base64 = new Base64();

Meteor.startup(() => {
  process.env.MAIL_URL = Meteor.settings.email;
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

  Accounts.emailTemplates.from = 'support_team@sancorseguros.com';
  Accounts.emailTemplates.siteName = 'JIRA WORKTIME';

  Accounts.emailTemplates.resetPassword = {
    subject(user) {
      return "Esqueci Senha";
    },
    text(user, url) {
      return `Olá! 
      Você recebeu este email, porque solicitou a alteração de senha do Jira WorkTime.
      Click no link abaixo para resetar a sua senha.

      ${url}
  
      Caso você não solicitou, por favor ignore esta mensagem.
  
  Obrigado,
  WorkTime Team.
  `
    },
    html(user, url) {
    }
  };

});

Meteor.methods({
  'logWork': function (userBase, issue, startedTime, tempoGasto, comentario) {
    const fetch = require('node-fetch');
    return fetch(Meteor.settings.jiraUrl + '/rest/api/2/issue/' + issue + '/worklog', {
      method: 'POST',
      headers: {
        'Authorization': 'Basic ' + userBase,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ "timeSpentSeconds": tempoGasto, "started": startedTime, "comment": comentario })
    })
      .then(response => {
        if (response.status == 201) {
          return response.status;
        } else {
          return response.json();
        }

      })
      .catch(err => console.error(err));
  },

  'checkIfUserExists': function (username) {
    return (Meteor.users.findOne({ username: username })) ? true : false;
  },

  'getDn': function () {
    return Meteor.settings.dn;
  },

  'getUserIssues': function (userBase) {

    const fetch = require('node-fetch');
    return fetch(Meteor.settings.jiraUrl + '/rest/api/3/search?jql=assignee="' + userBase.user + '"' + Meteor.settings.filter, {
      method: 'GET',
      headers: {
        'Authorization': 'Basic ' + userBase.base,
        'Accept': 'application/json'
      }
    })
      .then(response => {

        return response.json();
      })
      .catch(err => console.error(err));
  },

  'removerIssues': function () {
    return Tabela.remove({});
  },

});
